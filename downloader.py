import urllib
from lxml import etree
import os

def get_panopto_links(rss_link):
    tree = etree.parse(rss_link)
    guid_nodes = tree.xpath("//guid")
    result = [node.text for node in guid_nodes]
    return result


def download_links(links_list, folder, file_prefix, file_extension):
    base_path = os.path.join(folder, file_prefix)
    number_of_links = len(links_list)
    for index, link in enumerate(links_list):
        print "Downloading {i} / {t}".format(i=index+1, t=number_of_links)
        download_path = base_path + str(index+1) + file_extension
        urllib.urlretrieve(link, download_path)
        print "DONE"


if __name__ == '__main__':
    rss_link = "http://panoptotech.technion.ac.il/Panopto/Podcast/Podcast.ashx?courseid=978e6499-e763-4431-89ba-985970b13267&type=mp4"
    target_folder = "./"
    file_prefix = "video"
    file_extension = ".mp4"
    panopto_links = get_panopto_links(rss_link)
    download_links(panopto_links, target_folder, file_prefix, file_extension)
