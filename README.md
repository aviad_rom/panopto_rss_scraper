**README**

This downloader allows you to easily download all the available content from an RSS feed.
The XPath query is tailored for Panopto RSS files, and might vary in other RSS feeds.

I have implemented this tool as a practice of (really basic) web data extraction, and because I spend shitloads of time
on the train without proper WiFi during my last year of study so I needed to make good use of this time.



**REQUIREMENTS**

Python 2.7.9 and above (on 2.7.x numbering of course)
lxml (_pip install lxml_)
Fast internet connection (it takes time to download all the videos)


**USAGE**

Replace the "rss_link" variable in the file with your RSS link

Replace other variables if you wish

Commandline: python downloader.py


**USE AT YOUR OWN RISK! I DO NOT TAKE ANY RESPONSIBILIY FOR YOUR ACTIONS**
